Pod::Spec.new do |spec|

  spec.name         = "CDBaseFramework"
  spec.version      = "0.0.5"
  spec.summary      = "A CocoaPods library written in Swift"

  spec.description  = "Base classes A CocoaPods library written in Swift"

  spec.homepage     = "https://bitbucket.org/creativedevs/cdbaseframework"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "CreativeDevs" => "" }

  spec.ios.deployment_target = "11.0"
  spec.swift_version = "4.2"

  spec.source        = { :git => "https://bitbucket.org/creativedevs/cdbaseframework.git", :tag => "#{spec.version}" }

spec.framework = 'UIKit'
spec.dependency 'Alamofire'
spec.dependency 'Kingfisher', '4.10.0'
spec.dependency 'MOLH'
spec.dependency 'SwiftMessages', '6.0.0'
spec.source_files  = "CDBaseFramework/**/*.{h,m,swift}"

end