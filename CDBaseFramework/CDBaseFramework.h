//
//  CDBaseFramework.h
//  CDBaseFramework
//
//  Created by Waqas Ali on 17/04/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CDBaseFramework.
FOUNDATION_EXPORT double CDBaseFrameworkVersionNumber;

//! Project version string for CDBaseFramework.
FOUNDATION_EXPORT const unsigned char CDBaseFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CDBaseFramework/PublicHeader.h>


