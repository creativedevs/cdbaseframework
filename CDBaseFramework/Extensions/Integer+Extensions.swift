//
//  Integer+Extensions.swift


import Foundation

extension Int {
   public var boolValue: Bool { return self != 0 }
}
